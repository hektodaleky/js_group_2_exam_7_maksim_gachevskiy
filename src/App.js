import React, {Component} from "react";
import "./App.css";
import Item from "./components/Items/Item";
import OrderMenu from "./components/OrderMenu/OrderMenu";
import Total from "./components/OrderMenu/Total";
const menu = [
    {type: "dish", name: "Hamburger", id: "1"},
    {type: "dish", name: "CheeseBurger", id: "2"},
    {type: "dish", name: "Fries", id: "3"},
    {type: "drink", name: "Coffee", id: "4"},
    {type: "drink", name: "Tea", id: "5"},
    {type: "drink", name: "Cola", id: "6"}

];

const price = {
    Hamburger: 80,
    CheeseBurger: 90,
    Fries: 45,
    Coffee: 70,
    Tea: 50,
    Cola: 40
};

class App extends Component {

    state = {
        orders: {}
    };

    addDish = (item) => {
        if (!this.state.orders.hasOwnProperty(item)) {
            let orders = {...this.state.orders};
            orders[item] = 1;
            this.setState({orders});


        }
        else {
            let orders = {...this.state.orders};
            orders[item]++;
            this.setState({orders});
        }


    };
    removeDish=(item)=>{
        let orders = {...this.state.orders};
        orders[item]--;
        this.setState({orders});
    };

    render() {
        let total=0;
        for(let i in this.state.orders){
            total+=this.state.orders[i]*price[i];
        };

        return (
            <div className="Programm">
                <div className="orders">
                    {
                        Object.keys(this.state.orders).map(keys => {
                        let arr;

                        for (let i = 0; i < this.state.orders[keys]; i++) {


                            arr = (<OrderMenu key={keys} name={keys} count={this.state.orders[keys]}
                                              amount={this.state.orders[keys] * price[keys]} remove={()=>{
                                                  this.removeDish(keys);
                            }}></OrderMenu>)
                        }
                        return arr;
                    })}
                    <Total amount={total}/>







                </div>
                <div className="mainMenu">{

                    menu.map(index => {
                        return (

                            <Item type={index} click={() => {

                            this.addDish(index.name)
                        }} key={index.id} price={price[index.name]}/>)
                    })}</div>
            </div>
        );
    }
}

export default App;
