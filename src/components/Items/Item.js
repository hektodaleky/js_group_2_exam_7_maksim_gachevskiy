import React from "react";
import "./font-awesome-4.7.0/css/font-awesome.min.css";
import "./Item.css";


const getMenuIcon = one => {
    if (one === "dish")
        return (<i className="fa fa-cutlery" aria-hidden="true"></i>)
    else if (one === "drink")
        return <i className="fa fa-coffee" aria-hidden="true"></i>;
};
const getItem = (item,event,price) => {
    return (
        <div className="item" onClick={event} >
            <div className="onePart">{getMenuIcon(item.type)}</div>
            <div className="twoPart"><h5>{item.name}</h5>
                <p>{`price: ${price} KGS`}</p></div>

        </div>);
};
const Item = props => {
    return getItem(props.type,props.click,props.price);
};
export default Item;
