import React from 'react';
import './OrdersMenu.css';

const getTotalList=(amount)=>{
    if(amount>0)
        return(<p className="total">{`Total price `}<i className="amount"> {`${amount}KGS`}</i> </p>);
    else
        return <p className="total">{`Order is empty, please add some items`} </p>
};
const Total=props=>{


    return (
       getTotalList(props.amount)

    );
};
export default Total;
